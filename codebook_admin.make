; Codebook Admin Makefile

api = 2
core = 7.x

; Contrib Modules

projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.2

projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc5

projects[environment_indicator][subdir] = contrib
projects[environment_indicator][version] = 2.5

projects[module_filter][subdir] = contrib
projects[module_filter][version] = 2.0

projects[adminimal_admin_menu][subdir] = contrib
projects[adminimal_admin_menu][version] = 1.5

; Contrib Themes

projects[adminimal_theme][subdir] = contrib
projects[adminimal_theme][version] = 1.21
